# RUST CRUD API

### Things todo list

1. Clone this repository `git clone https://gitlab.com/hendisantika/rust-crud-api.git`
2. Navigate to the folder: `cd rust-crud-api`
3. Run this command: `rustc main.rs` then `./main`
4. Open your POSTMAN APP
5. Import POSTMAN Collection file
6. Test REST API

### List Commands

Add New User

```shell
curl --location 'http://localhost:8080/users' \
--header 'Content-Type: application/json' \
--data-raw '{
    "name": "Itadori Yuji",
    "email": "yuji@yopmail.com"
}'
```

### List all users

```shell
curl --location 'http://localhost:8080/users'
```

### Get User by ID

```shell
curl --location 'http://localhost:8080/users/1'
```

### Update User by ID

```shell
curl --location --request PUT 'http://localhost:8080/users/3' \
--header 'Content-Type: application/json' \
--data-raw '{
    "name": "Gojo Satoru",
    "email": "gojo@yopmail.com"
}'
```

### Delete User by ID

```shell
curl --location --request DELETE 'http://localhost:8080/users/4'
```
